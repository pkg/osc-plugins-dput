#!/usr/bin/python

import osc.cmdln as cmdln

import osc_plugin_dput  # noqa: F401


@cmdln.option('--maintained-in-git', action='store_true',
              help='add MAINTAINED_IN_GIT.txt')
@cmdln.option('--no-auto', action='store_true',
              help='don\'t guess the project name')
def do_dput(self, subcmd, opts, *args):
    """${cmd_name}: Automate the process of submitting a Debian package to the OBS server

        It expects a PROJECT_NAME and a .dsc, source .changes file, or
        source + binary .changes file. If used with a source + binary .changes
        file, the binaries will not be used.

        Usage:
            osc dput [PROJECT_NAME] PACKAGE.dsc
            osc dput [PROJECT_NAME] PACKAGE_source.changes

        Options:
            --maintained-in-git   add MAINTAINED_IN_GIT.txt
            --no-auto             don't guess the project name

        When the new upload does not introduce any changes, no commit is made
        and the successful exit code is returned.

        The project name can be omitted in the root of an osc checkout.
        Use --no-auto to prevent this behaviour.
    """

    # in older versions of osc, this ends up in the class namespace (!)
    if hasattr(self, "osc_plugin_dput"):
        osc_plugin_dput = self.osc_plugin_dput  # noqa: F811

    osc_plugin_dput.do_dput(self, subcmd, opts, *args)
