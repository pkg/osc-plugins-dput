osc dput
========

The dput plugin for Open Build Service commander is designed to help
quickly upload Debian source packages to OBS without needing to manually
check out the source, add new files, remove old files and commit it again.

`osc dput` automates the submission process, automagically extracting
the list of files and the commit message from the `.changes` file.

Installation
============

To install osc-plugin-clone, type:

    ./setup.py install

At the moment, installing to a user's home directory isn't supported.

License
=======

This program is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU General Public License version 2
text for more details.

You should have received a copy of the GNU General Public
License along with this package; if not, write to the Free
Software Foundation, Inc., 51 Franklin St, Fifth Floor,
Boston, MA  02110-1301 USA

Authors
=======

For the list of contributors, see CONTRIBUTORS.
